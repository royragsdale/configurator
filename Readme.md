# Configurator

[![pipeline status][pimg]][plnk] [![coverage report][cimg]][clnk] [![Go Report Card][rimg]][rlnk] [![GoDoc][dimg]][dlnk]

A simple, highly opinionated, library to load a configuration. Heavily based on
the recommendations from Kelsey Hightower's [12-fractured-apps][kh] post.  This
extends his library [envconfig][e] to add in the ability to also load from
a yaml configuration file.


This library has only one public function which expects to be passed a struct
specifying the configuration and containing the default values.

```
func LoadConfig(config interface{}, prefix string) error
```

This will the over-ride the defaults by loading with the following order (from
lowest priority to highest):

1. configuration file (yaml)
  - from `config.yml` (the library default, lowest priority)
  - from value specified by the CONFIG environment variable (with prefixing as
    described below)
2. environment variables
  - optionally specify a prefix, eg: `app` will result in `APP_VAR`
  - for more information on using environment variables see the `envconfig`
    [usage][u]

[kh]:https://medium.com/@kelseyhightower/12-fractured-apps-1080c73d481c
[e]:https://github.com/kelseyhightower/envconfig
[u]:https://github.com/kelseyhightower/envconfig#usage

[pimg]:https://gitlab.com/royragsdale/configurator/badges/master/pipeline.svg
[plnk]:https://gitlab.com/royragsdale/configurator/commits/master

[cimg]:https://gitlab.com/royragsdale/configurator/badges/master/coverage.svg
[clnk]:https://gitlab.com/royragsdale/configurator/commits/master

[rimg]:https://goreportcard.com/badge/gitlab.com/royragsdale/configurator
[rlnk]:https://goreportcard.com/report/gitlab.com/royragsdale/configurator

[dimg]:https://godoc.org/gitlab.com/royragsdale/configurator?status.svg
[dlnk]:https://godoc.org/gitlab.com/royragsdale/configurator



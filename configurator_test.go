package configurator

import (
	"os"
	"testing"
)

type Foo struct {
	A int
	B string
}

func TestOverrideFromFile(t *testing.T) {

	// load from default path
	foo := Foo{}
	overrideFromFile(&foo, "")
	if foo.A != 3 || foo.B != "default" {
		t.Errorf("load from default yaml incorrect: %v", foo)
	}

	// load from specified path
	foo = Foo{}
	os.Setenv(configPathEnvVar, "./test_configs/config.foo.specify.yml")
	overrideFromFile(&foo, "")
	if foo.A != 2 || foo.B != "specified" {
		t.Errorf("load from specified yaml incorrect: %v", foo)
	}

	// load non-existent config
	bar := Foo{}
	os.Setenv(configPathEnvVar, "./test_configs/config.NO.EXIST.yml")
	err := overrideFromFile(&bar, "")
	if bar != (Foo{}) || err == nil {
		t.Errorf("loading non existent file should error: %v", bar)
	}

	// clean up
	os.Unsetenv(configPathEnvVar)
}

func TestOverrideFromEnv(t *testing.T) {
	// clean up
	defer os.Unsetenv(configPathEnvVar)

	foo := Foo{}
	os.Setenv("A", "99")
	overrideFromEnv(&foo, "")
	if foo.A != 99 {
		t.Errorf("error loading int from env %v", foo)
	}
	os.Unsetenv("A")

	foo = Foo{}
	os.Setenv("A", "")
	overrideFromEnv(&foo, "")
	if foo.A != 0 {
		t.Errorf("error with '' for int from env %v", foo)
	}
	os.Unsetenv("A")

	foo = Foo{}
	os.Setenv("A", "thisIsNotAnInt")
	overrideFromEnv(&foo, "")
	if foo.A != 0 {
		t.Errorf("error with string for int from env %v", foo)
	}
	os.Unsetenv("A")

	foo = Foo{}
	os.Setenv("B", "env_string")
	overrideFromEnv(&foo, "")
	if foo.B != "env_string" {
		t.Errorf("error with string from env %v", foo)
	}
	os.Unsetenv("B")

	foo = Foo{}
	os.Setenv("B", "")
	overrideFromEnv(&foo, "")
	if foo.B != "" {
		t.Errorf("error with empty string from env %v", foo)
	}
	os.Unsetenv("B")

	// test prefix
	foo = Foo{}
	os.Setenv("APP_B", "env_string")
	overrideFromEnv(&foo, "APP")
	if foo.B != "env_string" {
		t.Errorf("error with string from env with prefix %v", foo)
	}
	os.Unsetenv("APP_B")

	foo = Foo{}
	os.Setenv("APP_B", "env_string")
	overrideFromEnv(&foo, "APP_")
	if foo.B != "" {
		t.Errorf("error should not match prefix %v", foo)
	}
	os.Unsetenv("APP_B")
}

func TestPrecedence(t *testing.T) {
	// all defaults
	foo := Foo{}
	LoadConfig(&foo, "")
	if foo.A != 3 || foo.B != "default" {
		t.Errorf("load from default yaml incorrect: %v", foo)
	}

	// specify yaml
	foo = Foo{}
	os.Setenv(configPathEnvVar, "./test_configs/config.foo.specify.yml")
	LoadConfig(&foo, "")
	if foo.A != 2 || foo.B != "specified" {
		t.Errorf("load from specified yaml incorrect: %v", foo)
	}

	// specify yaml but override with env
	foo = Foo{}
	os.Setenv(configPathEnvVar, "./test_configs/config.foo.specify.yml")
	os.Setenv("B", "env_var")
	LoadConfig(&foo, "")
	if foo.A != 2 || foo.B != "env_var" {
		t.Errorf("load from specified yaml and override with env incorrect: %v", foo)
	}

}

func TestInvalidYaml(t *testing.T) {
	foo := Foo{}

	os.Setenv(configPathEnvVar, "./test_configs/invalid.yml")
	err := LoadConfig(&foo, "")
	if err == nil {
		t.Errorf("should fail on invalid yaml config file: %v", err)
	}
}

func TestInvalidInput(t *testing.T) {

	// don't pass pointer (should fail)
	foo := Foo{}
	err := LoadConfig(foo, "")
	if err == nil {
		t.Errorf("should fail on non pointer: %v", err)
	}

	// pointer to non-struct (should fail)
	bar := []int{1, 2, 3, 4}
	err = LoadConfig(&bar, "")
	if err == nil {
		t.Errorf("should fail on pointer to non struct: %v", err)
	}

}

func TestToPrefixUpper(t *testing.T) {

	name := "a"
	if "A" != toPrefixedUpper(name, "") {
		t.Error("empty prefix should just capitalize")
	}

	name = "a"
	if "FOO_A" != toPrefixedUpper(name, "foo") {
		t.Error("should capitalize both")
	}

	name = "B"
	if "PRE_B" != toPrefixedUpper(name, "PRE") {
		t.Error("should capitalize both")
	}

}

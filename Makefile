
NAME := templatron

GO := go
GOFILES := $(wildcard *.go)
COVERFILE := coverage.out

# builds binary
.PHONY: pkg
pkg: 
	$(GO) build .

# tools are are useful for development or used in this Makefile
.PHONY: init
init:
	$(GO) get -u golang.org/x/lint/golint

.PHONY: test
test:
	$(GO) test .

.PHONY: cover
cover:
	$(GO) test -cover .

.PHONY: coverfunc
coverfunc:
	$(GO) test -coverprofile $(COVERFILE)
	go tool cover -html=$(COVERFILE) -o=coverage.html
	go tool cover -func=$(COVERFILE)

.PHONY: vet
vet:
	$(GO) vet .

.PHONY: lint
lint:
	golint -set_exit_status .

.PHONY: clean
clean:
	rm -f $(COVERFILE) coverage.html
	rm -f example/example

.PHONY: all
all: pkg cover vet lint

package main

import (
	"fmt"
	"os"

	"gitlab.com/royragsdale/configurator"
)

// This is the struct that specifies the configuration for the application
type exampleConfig struct {
	Host string
	Port int
}

func main() {

	// This is an example of setting configuration defaults
	config := exampleConfig{"127.0.0.1", 8000}
	fmt.Printf("my default config is: %v\n\n", config)

	// This shows an example overriding with a prefixed environement variable
	if _, ok := os.LookupEnv("EXAMPLE_CONFIG"); !ok {
		os.Setenv("EXAMPLE_CONFIG", "example.config.yml")
	}
	err := configurator.LoadConfig(&config, "example")
	if err != nil {
		fmt.Println("error loading config")
		os.Exit(1)
	}
	fmt.Println("when EXAMPLE_CONFIG=example.config.yml")
	fmt.Printf("my loaded config is: %v\n\n", config)

	// You can try over-ridding the config file either by using the CONFIG
	// enviornment variable to specify a yaml file or with environment
	// variables to specfiy fields.
	if configurator.LoadConfig(&config, "") != nil {
		fmt.Println("error loading config")
		os.Exit(1)
	}
	fmt.Println("try using CONFIG=./env.config.yaml to specify a file or HOST= to specify a field\n")
	fmt.Printf("my loaded config is: %v\n", config)
}

/*
Package configurator implements a simple library for loading configurations.

The library overrides values in a default configuration from the following sources
listed in increasing precedence.

A yaml configuration file (default, application specified, or passed with -config
command line flag).

From environmental variables, optionally with an application specific prefix.  Currently
the library only support loading int and string type values.

The goal is to gracefully handle enough common cases with minimal dependencies and complexity.
Getting a configuration loaded should be as simple as am single function call.
*/
package configurator

import (
	"errors"
	"io/ioutil"
	//"log"
	"os"
	"reflect"
	"strings"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

// defaultConfigPath is the lowest precedence file to load configuration from.
const defaultConfigPath = "config.yml"

// configPathEnvVar  is the environement variable tha can be used to load from a yaml file.
const configPathEnvVar = "CONFIG"

// loadYaml reads in a yaml file at path and unmarshalls it into the provided
// config.
func loadYaml(path string, config interface{}) error {

	configYaml, err := ioutil.ReadFile(path)
	if err != nil {
		//log.Printf("error loading config: %v ", err)
		return err
	}

	err = yaml.Unmarshal(configYaml, config)
	if err != nil {
		//log.Printf("error unmarshalling config: %v", err)
		return err
	}

	return nil
}

// overrideFromFile loads a specific yaml file based on a number of reasonable
// defaults.  Highest precedence is the CONFIG environement variable
// (optionally prefixed) and falling back to a defaultConfigPath.
func overrideFromFile(c interface{}, prefix string) error {
	var err error

	// check if a config path was specified in the environment
	env := toPrefixedUpper(configPathEnvVar, prefix)
	configPath, ok := os.LookupEnv(env)

	if ok && configPath != defaultConfigPath {
		// 1. Load from config file passed in with -config flag
		//log.Println("loading from cli specified path")
		err = loadYaml(configPath, c)
	} else {
		// 3. Load from default path (don't error in this case)
		//log.Println("loading from default path")
		loadYaml(defaultConfigPath, c)
	}

	return err
}

// getnEnvVar loads a value form the Environment by checking the uppercase of
// the configuration struct's field names. An application can also specify a prefix.
func toPrefixedUpper(name string, prefix string) string {
	if prefix != "" {
		varUpper := []string{strings.ToUpper(prefix), strings.ToUpper(name)}
		return strings.Join(varUpper, "_")
	}
	return strings.ToUpper(name)
}

// overrideFromEnv uses envconf to process environment variables into a
// configuration struct
func overrideFromEnv(c interface{}, prefix string) error {

	err := envconfig.Process(prefix, c)

	return err
}

// LoadConfig takes a default configuration and updates it with values specified in a file
// or passed through the environment. config must be a pointer to a struct and should contain
// the default configuration values.
func LoadConfig(config interface{}, prefix string) error {

	if reflect.ValueOf(config).Kind() != reflect.Ptr || reflect.ValueOf(config).Elem().Kind() != reflect.Struct {
		return errors.New("Requires pointer to struct passed in as default config")
	}

	err := overrideFromFile(config, prefix)
	if err != nil {
		return err
	}

	err = overrideFromEnv(config, prefix)

	return err
}
